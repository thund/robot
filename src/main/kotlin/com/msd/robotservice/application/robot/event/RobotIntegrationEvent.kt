package com.msd.robotservice.application.robot.event

sealed interface RobotIntegrationEvent {
  fun key(): String
}
