package com.msd.robotservice.domain.robot

object UpgradeValues {
  val storageByLevel = arrayOf(20, 50, 100, 200, 400, 1000)
  val maxHealthByLevel = arrayOf(10, 25, 50, 100, 200, 500)
  val attackDamageByLevel = arrayOf(1, 2, 5, 10, 20, 50)
  val miningSpeedByLevel = arrayOf(2, 5, 10, 15, 20, 40)
  val maxEnergyByLevel = arrayOf(20, 30, 40, 60, 100, 200)
  val energyRegenByLevel = arrayOf(4, 6, 8, 10, 15, 20)
}
