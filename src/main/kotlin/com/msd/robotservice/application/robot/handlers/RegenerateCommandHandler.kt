package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotRegeneratedIntegrationEvent
import com.msd.robotservice.domain.robot.RobotRepository
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class RegenerateCommandHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {

  @Transactional(rollbackOn = [Exception::class])
  fun handle(command: RobotCommand) {
    val robot = robotRepository.findByIdOrThrow(command.robotId)
    robot.regenerate()
    this.robotRepository.save(robot)

    val event = RobotRegeneratedIntegrationEvent.build(robot)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
