package com.msd.robotservice.application.robot

import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.event.ApplicationEvents
import org.springframework.test.context.event.RecordApplicationEvents
import java.util.*

@RecordApplicationEvents
class RobotRepositoryIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  lateinit var robotRepository: RobotRepository

  @Autowired
  lateinit var applicationEvents: ApplicationEvents

  private val robot1 = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))

  @Test
  fun `repository save should result in event publication`() {
    robotRepository.save(robot1)
    assertThat(applicationEvents.stream())
      .hasSizeGreaterThan(0)
  }

  @Test
  fun `should clear events`() {
    robotRepository.save(robot1)
    assertThat(robot1.domainEvents()).isEmpty()
  }
}
