package com.msd.robotservice.domain.robot

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.event.*
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

internal class RobotTest {

  private val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))

  @BeforeEach
  fun setup() {
    robot.clearEvents()
  }

  @Test
  fun `when move from planet Should reduce energy`() {
    val toPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID(), 2)

    robot.move(toPlanet)

    assertThat(robot.energy).isEqualTo(18)
  }

  @Test
  fun `when move from planet Should add movement event`() {
    val toPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID())

    robot.move(toPlanet)

    assertThat(robot.domainEvents())
      .hasSize(2)
      .hasExactlyElementsOfTypes(RobotEnergyUpdated::class.java, RobotMoved::class.java)
  }

  @Test
  fun `when receive damage should reduce health`() {
    robot.receiveDamage(5)

    assertThat(robot.health).isEqualTo(5)
  }

  @Test
  fun `when receive damage below health should kill robot`() {
    robot.receiveDamage(robot.health + 10)

    assertThat(robot.alive).isFalse
  }

  @Test
  fun `when receive damage below health should add event`() {
    robot.receiveDamage(5)

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotHealthUpdated::class.java)
  }

  @Test
  fun `when receive damage below health should add kill event`() {
    robot.receiveDamage(robot.health)

    assertThat(robot.domainEvents())
      .hasSize(2)
      .hasExactlyElementsOfTypes(RobotHealthUpdated::class.java, RobotKilled::class.java)
  }

  @Test
  fun `when kill should kill robot`() {
    robot.kill()

    assertThat(robot.alive).isFalse
  }

  @Test
  fun `when reduce energy should add event`() {
    robot.kill()

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotKilled::class.java)
  }

  @Test
  fun `when attack robot not on planet Should throw `() {
    val target = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    assertThrows<TargetRobotOutOfReachException> { robot.attack(target) }
  }

  @Test
  fun `when attack dead robot Should throw `() {
    val target = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    target.kill()
    assertThrows<Exception> { robot.attack(target) }
  }


  @Test
  fun `when attack robot Should reduce energy`() {
    val target = Robot.of(UUID.randomUUID(), robot.planet)

    robot.attack(target)

    assertThat(robot.energy).isEqualTo(19)
  }

  @Test
  fun `when attack robot Should receive damage`() {
    val target = Robot.of(UUID.randomUUID(), robot.planet)

    robot.attack(target)

    assertThat(target.health).isEqualTo(9)
  }

  @Test
  fun `when attack robot Should add event`() {
    val target = Robot.of(UUID.randomUUID(), robot.planet)
    target.clearEvents()

    robot.attack(target)

    assertThat(robot.domainEvents())
      .hasSize(2)
      .hasExactlyElementsOfTypes(RobotEnergyUpdated::class.java, RobotAttacked::class.java)
    assertThat(target.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotHealthUpdated::class.java)
  }

  @Test
  fun `when upgrade robot Should add event`() {
    robot.upgrade(UpgradeType.DAMAGE, 1)

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotUpgraded::class.java)
  }

  @Test
  fun `when repair robot Should add event`() {
    robot.repair()

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotHealthUpdated::class.java)
  }

  @Test
  fun `when regenerate robot Should add event`() {
    robot.regenerate()

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotEnergyUpdated::class.java)
  }

  @Test
  fun `when addResource Should add event`() {
    robot.addResource(ResourceType.PLATIN, 1)

    assertThat(robot.domainEvents())
      .hasSize(2)
      .hasExactlyElementsOfTypes(RobotResourceMined::class.java, RobotInventoryUpdated::class.java)
  }

  @Test
  fun `when takeAllResources Should add event`() {
    robot.takeAllResources()

    assertThat(robot.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(RobotInventoryUpdated::class.java)
  }

  @Test
  fun `should maintain event ordering`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    val toPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val target = Robot.of(UUID.randomUUID(), toPlanet)

    robot.move(toPlanet)
    robot.attack(target)
    robot.kill()

    assertThat(robot.domainEvents())
      .hasSize(6)
      .hasExactlyElementsOfTypes(
        RobotSpawned::class.java,
        RobotEnergyUpdated::class.java, // first reduce energy
        RobotMoved::class.java, // then move
        RobotEnergyUpdated::class.java, // first reduce energy
        RobotAttacked::class.java, // then attack
        RobotKilled::class.java
      )
  }
}
