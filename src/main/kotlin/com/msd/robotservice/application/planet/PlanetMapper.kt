package com.msd.robotservice.application.planet

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetType
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(componentModel = "spring")
abstract class PlanetMapper {

  @Mappings(
    Mapping(target = "planetId", source = "planet.planetId"),
    Mapping(target = "movementDifficulty", source = "movementCost"),
    Mapping(target = "planetType", source = "planetType"),
    Mapping(target = "resourceType", source = "planet.resourceType")
  )
  abstract fun planetToPlanetDTO(
    planet: Planet,
    movementCost: Int,
    planetType: PlanetType
  ): PlanetDto
}
