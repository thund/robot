package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotsRevealedIntegrationEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import java.util.*

internal class RevealRobotsHandlerTest {
  private val robotRepository: RobotRepository = mock()
  private val eventPublisher: RobotIntegrationEventPublisher = mock()
  private val revealRobotsHandler = RevealRobotsHandler(robotRepository, eventPublisher)

  @Test
  fun shouldPublish() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    val robots = listOf(robot)
    whenever(robotRepository.findAllAlive()).thenReturn(robots)
    revealRobotsHandler.handle()

    val captor = argumentCaptor<RobotsRevealedIntegrationEvent>()
    verify(eventPublisher).publish(captor.capture(), anyOrNull())
    val event = captor.firstValue
    assertThat(event.robots).hasSize(1)
  }
}
