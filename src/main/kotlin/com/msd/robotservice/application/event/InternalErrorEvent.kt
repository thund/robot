package com.msd.robotservice.application.event

import java.util.*

data class InternalErrorEvent(
  override val playerId: UUID,
  override val transactionId: UUID,
  override val robotId: UUID,
  override val description: String = "An internal error occurred",
  override val details: String
) : ErrorEvent(
  playerId, transactionId, robotId,
  "ERR_ROBOT_INTERNAL_ERROR",
  description,
  details
)
