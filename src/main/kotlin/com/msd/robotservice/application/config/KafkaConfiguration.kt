package com.msd.robotservice.application.config

import com.msd.robotservice.application.config.properties.TopicConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.KafkaAdmin.NewTopics
import org.springframework.kafka.listener.CommonLoggingErrorHandler
import org.springframework.kafka.support.converter.ByteArrayJsonMessageConverter

@Configuration
class KafkaConfiguration {
  @Bean
  fun byteArrayJsonMessageConverter(): ByteArrayJsonMessageConverter {
    return ByteArrayJsonMessageConverter()
  }

  // TODO: This error handler fails when a bad record (e.g. unsupported compression) is consumed.
  //  In that case the error handler will be stuck in an endless loop and won't commit any offsets.
  //  We hope, that this edge-case is negligible
  @Bean
  fun errorHandler(): CommonLoggingErrorHandler {
    return CommonLoggingErrorHandler()
  }

  @Bean
  fun robotTopics(config: TopicConfigurationProperties): NewTopics {
    return NewTopics(
      *config.config.entries.map { (key, value) ->
        TopicBuilder.name(key)
          .partitions(value.partitions)
          .replicas(value.replicas)
          .configs(value.properties)
          .build()
      }.toTypedArray()
    )
  }
}
