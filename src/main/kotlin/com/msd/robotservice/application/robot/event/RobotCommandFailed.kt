package com.msd.robotservice.application.robot.event

import com.msd.robotservice.application.event.ErrorEvent
import java.util.*

data class RobotCommandFailed(
  override val playerId: UUID,
  override val transactionId: UUID,
  override val robotId: UUID,
  override val description: String
) : ErrorEvent(playerId, transactionId, robotId, "ERR_ROBOT_COMMAND_FAILED", description)
