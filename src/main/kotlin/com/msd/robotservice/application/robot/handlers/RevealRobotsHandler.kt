package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotsRevealedIntegrationEvent
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class RevealRobotsHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    val robots = robotRepository.findAllAlive()

    val event = RobotsRevealedIntegrationEvent.build(robots)
    this.integrationEventPublisher.publish(event)
  }
}
