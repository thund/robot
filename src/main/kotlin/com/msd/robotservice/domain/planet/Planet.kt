package com.msd.robotservice.domain.planet

import com.fasterxml.jackson.annotation.JsonIgnore
import com.msd.robotservice.domain.core.AbstractAggregateRoot
import com.msd.robotservice.domain.planet.event.PlanetDiscovered
import com.msd.robotservice.domain.planet.event.PlanetNeighbourAdded
import com.msd.robotservice.domain.planet.event.PlanetNeighbourRemoved
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
data class Planet private constructor(
  @Id
  @Type(type = "uuid-char")
  val planetId: UUID,
  @Type(type = "uuid-char")
  val gameWorldId: UUID,
  var movementDifficulty: Int = 1,
  var resourceType: ResourceType? = null,
  @JsonIgnore
  @ManyToMany(fetch = FetchType.EAGER)
  var neighbours: MutableList<Planet> = mutableListOf()
) : AbstractAggregateRoot() {
  companion object {
    /**
     * Fabric method to create a new planet.
     * Adds discovering event.
     */
    fun of(
      planetId: UUID,
      gameWorldId: UUID,
      movementDifficulty: Int = 1,
      resourceType: ResourceType? = null,
      neighbours: MutableList<Planet> = mutableListOf()
    ): Planet {
      val planet = Planet(planetId, gameWorldId, movementDifficulty, resourceType, neighbours)
      planet.events.add(PlanetDiscovered(planet))
      return planet
    }
  }

  fun addNeighbour(planet: Planet) {
    this.neighbours.add(planet)
    this.events.add(PlanetNeighbourAdded(planetId, planet))
  }

  fun removeNeighbour(planet: Planet) {
    this.neighbours.removeIf { it == planet || it.planetId == planet.planetId }
    this.events.add(PlanetNeighbourRemoved(planetId, planet))
  }
  fun isNeighbourTo(planetId: UUID) = this.neighbours.any { it.planetId == planetId }
  override fun toString(): String {
    return "Planet(planetId=$planetId, gameWorldId=$gameWorldId, movementDifficulty=$movementDifficulty, resourceType=$resourceType, neighbours=${neighbours.map { it.planetId }})"
  }
}
