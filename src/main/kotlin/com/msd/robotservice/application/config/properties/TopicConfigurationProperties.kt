package com.msd.robotservice.application.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("topics")
data class TopicConfigurationProperties(
  val config: Map<String, TopicConfiguration>
) {
  data class TopicConfiguration(
    val partitions: Int = 1,
    val replicas: Int = 1,
    val properties: Map<String, String> = mapOf()
  )
}
