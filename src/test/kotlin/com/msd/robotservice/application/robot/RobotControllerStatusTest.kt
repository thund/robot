package com.msd.robotservice.application.robot

import com.msd.robotservice.application.robot.handlers.SpawnRobotHandler
import com.msd.robotservice.application.robot.mappers.RobotMapper
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.RobotNotFoundException
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.*

// TODO: Refactor tests. At the moment they are highly procedural and only test the basic
//  functionality in terms of response codes. No response body is being checked and we can maybe
//  find a smarter solution than testing every endpoint manually.

@WebFluxTest(controllers = [RobotController::class])
class RobotControllerStatusTest {
  @MockBean
  lateinit var robotRepository: RobotRepository

  @MockBean
  lateinit var robotMapper: RobotMapper

  @MockBean
  lateinit var spawnRobotHandler: SpawnRobotHandler

  @Autowired
  lateinit var webTestClient: WebTestClient

  @Test
  fun `should return 201 on spawn`() {
    webTestClient.post()
      .uri("/robots")
      .randomRobotBody()
      .accept(MediaType.APPLICATION_JSON)
      .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
      .exchange()
      .expectStatus().isCreated
  }

  @Test
  fun `should return 200 on get robots`() {
    webTestClient.get()
      .uri { builder ->
        builder.path("/robots")
          .queryParam("player-id", UUID.randomUUID()).build()
      }
      .accept(MediaType.APPLICATION_JSON)
      .exchange()
      .expectStatus().isOk
  }

  @Test
  fun `should return 404 on get robot When robot not existent`() {
    whenever(robotRepository.findById(any())) doReturn Optional.empty()

    webTestClient.get()
      .uri("/robots/{id}", UUID.randomUUID())
      .accept(MediaType.APPLICATION_JSON)
      .exchange()
      .expectStatus().isNotFound
  }

  @Test
  fun `should return 200 on get robot`() {
    whenever(robotRepository.findById(any())) doReturn Optional.of(mock())

    webTestClient.get()
      .uri("/robots/{id}", UUID.randomUUID())
      .accept(MediaType.APPLICATION_JSON)
      .exchange()
      .expectStatus().isOk
  }

  @Test
  fun `should return 404 on get robot`() {
    whenever(robotRepository.findById(any())) doReturn Optional.of(mock())

    webTestClient.get()
      .uri("/robots/{id}", UUID.randomUUID())
      .accept(MediaType.APPLICATION_JSON)
      .exchange()
      .expectStatus().isOk
  }
}

private fun WebTestClient.RequestBodySpec.randomRobotBody(): WebTestClient.RequestHeadersSpec<*> {
  return this.bodyValue(
    """
        {
          "transactionId": "${UUID.randomUUID()}",
          "player": "${UUID.randomUUID()}",
          "planets": [
            "${UUID.randomUUID()}"
           ],
           "quantity": 1
        }
    """.trimIndent()
  )
}

private fun WebTestClient.RequestBodySpec.randomUpgrade(): WebTestClient.RequestHeadersSpec<*> {
  return this.bodyValue(
    """
        {
          "transactionId": "${UUID.randomUUID()}",
          "upgradeType": "STORAGE",
          "targetLevel": 1
        }
    """.trimIndent()
  )
}

private fun WebTestClient.RequestBodySpec.randomItem(): WebTestClient.RequestHeadersSpec<*> {
  return this.bodyValue(
    """
        {
          "transactionId": "${UUID.randomUUID()}",
          "itemType": "WORMHOLE"
        }
    """.trimIndent()
  )
}

private fun WebTestClient.RequestBodySpec.randomRestoration(): WebTestClient.RequestHeadersSpec<*> {
  return this.bodyValue(
    """
        {
          "transactionId": "${UUID.randomUUID()}",
          "restorationType": "HEALTH"
        }
    """.trimIndent()
  )
}
