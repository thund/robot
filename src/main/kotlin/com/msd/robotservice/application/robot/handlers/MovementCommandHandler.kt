package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotMovedIntegrationEvent
import com.msd.robotservice.application.robot.exception.TargetPlanetNotReachableException
import com.msd.robotservice.application.robot.exception.UnknownPlanetException
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class MovementCommandHandler(
  val robotRepository: RobotRepository,
  val planetRepository: PlanetRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {

  companion object {
    val logger = KotlinLogging.logger {}
  }

  @Transactional(rollbackOn = [Exception::class])
  fun handle(command: RobotCommand) {
    val robotId = command.robotId
    val planetId = command.planetId ?: throw RuntimeException("PlanetId is null")

    val robot = robotRepository.findByIdOrThrow(robotId)
    val sourcePlanet = robot.planet

    val isNeighbour = sourcePlanet.isNeighbourTo(planetId)
    if (!isNeighbour) {
      throw TargetPlanetNotReachableException("Planet $planetId is not a neighbour of ${robot.planet.planetId}")
    }

    val targetPlanet =
      planetRepository.findById(planetId).orElseThrow { UnknownPlanetException(planetId) }

    robot.move(targetPlanet)
    this.robotRepository.save(robot)

    val event = RobotMovedIntegrationEvent.build(robot, sourcePlanet, targetPlanet)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
