package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotSpawnedIntegrationEvent(
  val robot: Robot,
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot) =
      RobotSpawnedIntegrationEvent(
        robot = robot
      )
  }

  override fun key(): String = robot.id.toString()
}
