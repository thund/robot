package com.msd.robotservice.application.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConfigurationProperties(prefix = "microservice.map")
@ConstructorBinding
data class MicroserviceMapConfigurationProperties(val address: String)
