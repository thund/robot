package com.msd.robotservice.infrastructure.game

import com.msd.robotservice.application.robot.handlers.PruneRobotsHandler
import com.msd.robotservice.application.robot.handlers.RevealRobotsHandler
import com.msd.robotservice.infrastructure.game.dto.GameStatus
import com.msd.robotservice.infrastructure.game.dto.GameStatusEvent
import com.msd.robotservice.infrastructure.game.dto.RoundStatus
import com.msd.robotservice.infrastructure.game.dto.RoundStatusEvent
import mu.KotlinLogging
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component

@Component
class GameEventListener(
  val pruneRobotsHandler: PruneRobotsHandler,
  val revealRobotsHandler: RevealRobotsHandler,
) {
  val logger = KotlinLogging.logger { }

  @KafkaListener(topics = ["status"], groupId = "robot", autoStartup = "true")
  fun gameStatusListener(
    @Payload event: GameStatusEvent
  ) {
    if(event.status == GameStatus.ENDED) {
      pruneRobotsHandler.handle()
    }

  }

  @KafkaListener(topics = ["roundStatus"], groupId = "robot", autoStartup = "true")
  fun roundStatusListener(
    @Payload event: RoundStatusEvent
  ) {
    if(event.roundStatus == RoundStatus.ENDED) {
      revealRobotsHandler.handle()
    }
  }

}
