package com.msd.robotservice.loadtest

import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import java.util.*
import javax.transaction.Transactional
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@Transactional
@Disabled("Load testing should be run manually")
@OptIn(ExperimentalTime::class)
class RobotLoadTest(
  @Autowired
  private val robotRepository: RobotRepository
) : AbstractIntegrationTest() {

  val logger = KotlinLogging.logger {}

  @Test
  fun `write 100 robots to the repo and retrieve one`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robotRepository.save(robot)
    for (i in 1..99) {
      robotRepository.save(Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID())))
    }

    val robots = robotRepository.findAll().toList()
    Assertions.assertEquals(100, robots.size)

    var repoRobot: Robot?
    val timePassed: Duration = measureTime {
      repoRobot = robotRepository.findByIdOrNull(robot.id)!!
    }
    logger.info("$timePassed has passed while looking for the specified Robot")
    assertNotNull(repoRobot)
  }

  @Test
  fun `write 1000 robots to the repo and retrieve one`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robotRepository.save(robot)
    for (i in 1..999) {
      robotRepository.save(Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID())))
    }

    val robots = robotRepository.findAll().toList()
    Assertions.assertEquals(1000, robots.size)

    var repoRobot: Robot?
    val timePassed: Duration = measureTime {
      repoRobot = robotRepository.findByIdOrNull(robot.id)!!
    }
    logger.info("$timePassed has passed while looking for the specified Robot")
    assertNotNull(repoRobot)
  }

  @Test
  fun `write 10000 robots to the repo and retrieve one`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robotRepository.save(robot)
    for (i in 1..9999) {
      robotRepository.save(Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID())))
    }

    val robots = robotRepository.findAll().toList()
    Assertions.assertEquals(10000, robots.size)

    var repoRobot: Robot?
    val timePassed = measureTime {
      repoRobot = robotRepository.findByIdOrNull(robot.id)!!
    }
    logger.info("$timePassed has passed while looking for the specified Robot")
    assertNotNull(repoRobot)
  }
}
