package com.msd.robotservice.infrastructure.trading.dto

enum class TradableType {
  RESOURCE,
  UPGRADE,
  RESTORATION,
  ITEM
}
