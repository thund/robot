package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotUpgradedIntegrationEvent
import com.msd.robotservice.application.robot.event.RobotsRevealedIntegrationEvent
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.UpgradeType
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class PruneRobotsHandler(
  val robotRepository: RobotRepository
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    robotRepository.deleteAll()
  }
}
