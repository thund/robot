package com.msd.robotservice.domain.robot.event

import java.util.*

data class RobotEnergyUpdated(
  private val playerId: UUID,
  val robot: UUID,
  val amount: Int,
  val energy: Int
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
