package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotSpawnedIntegrationEvent
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class SpawnRobotHandler(
  val planetRepository: PlanetRepository,
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(player: UUID, transactionId: UUID? = null) : Robot {
    val spawnPlanet = planetRepository.getRandomPlanet()
    val robot = Robot.of(player, spawnPlanet)

    logger.info("Spawned new robot with id ${robot.id} for player $player")

    this.robotRepository.save(robot)

    val event = RobotSpawnedIntegrationEvent.build(robot)
    this.integrationEventPublisher.publish(event, transactionId)

    return robot
  }
}
