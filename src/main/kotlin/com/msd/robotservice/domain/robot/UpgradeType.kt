package com.msd.robotservice.domain.robot

enum class UpgradeType {
  STORAGE,
  HEALTH,
  DAMAGE,
  MINING_SPEED,
  MINING,
  MAX_ENERGY,
  ENERGY_REGEN
}
