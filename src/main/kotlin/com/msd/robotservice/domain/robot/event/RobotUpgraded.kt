package com.msd.robotservice.domain.robot.event

import com.msd.robotservice.domain.robot.UpgradeType
import java.util.*

data class RobotUpgraded(
  private val playerId: UUID,
  val robot: UUID,
  val type: UpgradeType,
  val level: Int
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
