package com.msd.robotservice.domain.robot.event

import com.msd.robotservice.domain.planet.ResourceType
import java.util.*

data class RobotResourceMined(
  private val playerId: UUID,
  val robot: UUID,
  val resourceType: ResourceType,
  val amount: Int
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
