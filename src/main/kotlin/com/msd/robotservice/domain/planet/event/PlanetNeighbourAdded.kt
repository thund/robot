package com.msd.robotservice.domain.planet.event

import com.msd.robotservice.domain.planet.Planet
import java.util.*

data class PlanetNeighbourAdded(
  val planet: UUID,
  val neighbour: Planet
) : PlanetDomainEvent
