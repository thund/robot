package com.msd.robotservice.infrastructure.game

import com.fasterxml.jackson.databind.ObjectMapper
import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.application.robot.event.RobotsRevealedIntegrationEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.producer.ProducerRecord
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.listener.ContainerProperties
import org.springframework.kafka.listener.KafkaMessageListenerContainer
import org.springframework.kafka.listener.MessageListener
import org.springframework.kafka.test.utils.ContainerTestUtils
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit


//@DirtiesContext
internal class GameEventListenerIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  lateinit var kafkaTemplate: KafkaTemplate<String, String>

  @Autowired
  lateinit var robotRepository: RobotRepository

  @Autowired
  lateinit var objectMapper: ObjectMapper

  @Autowired
  lateinit var consumerFactory: DefaultKafkaConsumerFactory<String?, ByteArray>

  @BeforeEach
  fun setUp() {
    robotRepository.deleteAll()
  }

  @Test
  fun `should delete all robots at the end of game`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robotRepository.save(robot)
    assertThat(robotRepository.findAll().toList()).hasSize(1)

    val gameId = UUID.randomUUID()
    val message = """
      {
        "gameId": "$gameId",
        "status": "ended"
      }
    """.trimIndent()
    val record =
      ProducerRecord("status", gameId.toString(), message)
    kafkaTemplate.send(record).addCallback({}, { throw it })

    await().atMost(10, TimeUnit.SECONDS)
      .until { robotRepository.findAll().toList().isEmpty() }
  }

  @Test
  @Disabled
  // TODO: I don't understand why this test is failing
  fun `should reveal all robots at the end of round`() {
    val robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robot.clearEvents()
    robotRepository.save(robot)
    assertThat(robotRepository.findAll().toList()).hasSize(1)

    val message = """
      {
        "gameId": "5bc9f935-32f1-4d7b-a90c-ff0e6e34125a",
        "roundId": "5bc9f935-32f1-4d7b-a90c-ff0e6e34125a",
        "roundNumber": 0,
        "roundStatus": "ended"
      }
    """.trimIndent()
    val producerRecord =
      ProducerRecord("roundStatus", "", message)
    kafkaTemplate.send(producerRecord).addCallback({}, { throw it })

    // Setup consumer
    val consumerRecords: BlockingQueue<ConsumerRecord<String?, ByteArray>> = LinkedBlockingQueue()
    val container: KafkaMessageListenerContainer<String?, ByteArray>

    val containerProperties = ContainerProperties("robot.integration")
    container = KafkaMessageListenerContainer<String?, ByteArray>(
      consumerFactory,
      containerProperties
    )
    container.setupMessageListener(MessageListener { record ->
      if (record.headers().lastHeader("type").value()
          .toString(Charsets.UTF_8) == "RobotsRevealedIntegrationEvent"
      ) {
        consumerRecords.add(record)
      }
    })
    container.start()
    ContainerTestUtils.waitForAssignment(container, 1)

    val consumerRecord = consumerRecords.poll(10, TimeUnit.SECONDS)
    assertThat(consumerRecord).isNotNull
    val event =
      objectMapper.readValue(consumerRecord.value(), RobotsRevealedIntegrationEvent::class.java)
    assertThat(event.robots).hasSize(1)
    assertThat(event.robots[0].robotId).isEqualTo(robot.id)
    assertThat(event.robots[0].planetId).isEqualTo(robot.planet.planetId)
    assertThat(event.robots[0].levels.damageLevel).isEqualTo(robot.damageLevel)
    assertThat(event.robots[0].levels.healthLevel).isEqualTo(robot.healthLevel)
    container.stop()
  }
}
