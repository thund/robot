package com.msd.robotservice.domain.core

/**
 * Marker interface for a domain event
 */
interface DomainEvent
