package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotResourceMinedIntegrationEvent(
  val robotId: UUID,
  val minedResource: ResourceType,
  val minedAmount: Int,
  val resourceInventory: Map<ResourceType, Int>
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot, minedResource: ResourceType, minedAmount: Int) =
      RobotResourceMinedIntegrationEvent(
        robotId = robot.id,
        minedResource = minedResource,
        minedAmount = minedAmount,
        resourceInventory = robot.inventory.resources
      )
  }

  override fun key(): String = robotId.toString()
}
