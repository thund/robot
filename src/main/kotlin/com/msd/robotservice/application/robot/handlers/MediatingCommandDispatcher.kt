package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.event.InternalErrorEvent
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class MediatingCommandDispatcher(
  private val fightingCommandHandler: FightingCommandHandler,
  private val miningCommandHandler: MiningCommandHandler,
  private val movementCommandHandler: MovementCommandHandler,
  private val regenerateCommandHandler: RegenerateCommandHandler,
  private val applicationEventPublisher: ApplicationEventPublisher
) {

  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handleList(commands: List<RobotCommand>) {
    commands.forEach {
      try {
        handle(it)
      } catch (e: Exception) {
        // We need to ensure that every exception is caught, otherwise the whole command list fails
        logger.error(e) { "Error while executing command: $it" }
        this.applicationEventPublisher.publishEvent(
          InternalErrorEvent(
            it.playerId,
            it.transactionId,
            it.robotId,
            e.message ?: "Unhandled error while executing command",
            e.stackTraceToString()
          )
        )
      }
    }
  }

  fun handle(command: RobotCommand) {
    // Exhaustive pattern matching for enums will be default in Kotlin 1.7.
    //  Therefore we will get compile time errors if we forget to handle a command type.
    when (command.command) {
      RobotCommand.CommandType.BATTLE -> fightingCommandHandler.handle(command)
      RobotCommand.CommandType.MINING -> miningCommandHandler.handle(command)
      RobotCommand.CommandType.MOVEMENT -> movementCommandHandler.handle(command)
      RobotCommand.CommandType.REGENERATE -> regenerateCommandHandler.handle(command)
    }
  }
}
