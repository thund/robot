package com.msd.robotservice.domain.planet

import com.msd.robotservice.domain.planet.event.PlanetDiscovered
import com.msd.robotservice.domain.planet.event.PlanetNeighbourAdded
import com.msd.robotservice.domain.planet.event.PlanetNeighbourRemoved
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*

internal class PlanetTest {
  internal val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())

  @BeforeEach
  fun setup() {
    planet.clearEvents()
  }

  @Test
  fun `when planet created should add event`() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    assertThat(planet.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(PlanetDiscovered::class.java)
  }

  @Test
  fun `when planet add neighbour should add event`() {
    val neighbour = Planet.of(UUID.randomUUID(), UUID.randomUUID())

    planet.addNeighbour(neighbour)

    assertThat(this.planet.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(PlanetNeighbourAdded::class.java)
  }

  @Test
  fun `when planet remove neighbour should add event`() {
    val neighbour = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    planet.addNeighbour(neighbour)
    planet.clearEvents()

    planet.removeNeighbour(neighbour)

    assertThat(this.planet.domainEvents())
      .hasSize(1)
      .hasExactlyElementsOfTypes(PlanetNeighbourRemoved::class.java)
  }
}
