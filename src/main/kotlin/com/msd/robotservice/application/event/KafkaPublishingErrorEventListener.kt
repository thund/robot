package com.msd.robotservice.application.event

import com.fasterxml.jackson.databind.ObjectMapper
import mu.KotlinLogging
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.context.event.EventListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.time.Instant
import java.time.format.DateTimeFormatter

@Component
class KafkaPublishingErrorEventListener(
  val kafkaTemplate: KafkaTemplate<String, String>,
  val objectMapper: ObjectMapper
) {
  private val dateTimeFormatter = DateTimeFormatter.ISO_INSTANT
  private val logger = KotlinLogging.logger { }

  companion object {
    private const val errorTopic = "error"
  }

  @EventListener(ErrorEvent::class)
  fun publish(event: ErrorEvent) {
    val type = event::class.simpleName
    logger.debug { "Received error Event of type: $type. Publishing..." }

    val record =
      ProducerRecord<String, String>(
        errorTopic,
        event.playerId.toString(),
        objectMapper.writeValueAsString(event)
      )
    if (type != null) {
      record.headers().add("type", type.encodeToByteArray())
    }
    record.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).encodeToByteArray())
    record.headers().add(
      "playerId",
      if (event.playerId != null) event.playerId.toString().encodeToByteArray() else null
    )

    kafkaTemplate.send(record).addCallback({
      logger.debug { "Successfully sent record ${it?.producerRecord?.key()}" }
    }, {
      logger.error(it) { "Failed sending record" }
      throw it
    })
  }
}
