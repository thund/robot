package com.msd.robotservice.domain.planet.event

import com.msd.robotservice.domain.core.DomainEvent

sealed interface PlanetDomainEvent : DomainEvent
