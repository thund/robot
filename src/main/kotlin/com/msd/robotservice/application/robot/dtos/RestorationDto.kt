package com.msd.robotservice.application.robot.dtos

import com.msd.robotservice.application.robot.RestorationType
import java.util.*

data class RestorationDto(
  val transactionId: UUID,
  val restorationType: RestorationType
)
