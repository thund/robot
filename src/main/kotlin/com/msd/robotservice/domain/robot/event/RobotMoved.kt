package com.msd.robotservice.domain.robot.event

import java.util.*

data class RobotMoved(
  private val playerId: UUID,
  val robot: UUID,
  val fromPlanet: UUID,
  val toPlanet: UUID
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
