package com.msd.robotservice.application.robot.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.msd.robotservice.application.core.DungeonMessageHeaders
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

@Service
class RobotIntegrationEventKafkaPublisher(
  val objectMapper: ObjectMapper,
  val kafkaTemplate: KafkaTemplate<String, String>,
  val robotRepository: RobotRepository
) : RobotIntegrationEventPublisher {
  companion object {
    val logger = KotlinLogging.logger {}
    val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ISO_INSTANT

    const val ROBOT_TOPIC = "robot.integration"
  }


  override fun publish(event: RobotIntegrationEvent, transactionId: UUID?) {
    val key = event.key()
    val recipients = getRecipients(event)
    val record = buildProducerRecord(key, event, recipients, transactionId)
    send(record)
  }

  private fun send(record: ProducerRecord<String, String>) {
    kafkaTemplate.send(record).addCallback({
      logger.debug { "Successfully sent record ${it?.producerRecord?.key()}" }
    }, {
      logger.error(it) { "Failed sending record" }
      throw it
    })
  }

  private fun buildProducerRecord(
    key: String,
    event: Any,
    recipients: List<UUID>,
    correlationId: UUID? = null
  ): ProducerRecord<String, String> {
    val record =
      ProducerRecord(ROBOT_TOPIC, key, objectMapper.writeValueAsString(event))
    correlationId.apply {
      record.headers()
        .add( DungeonMessageHeaders.TRANSACTION_ID, this.toString().toByteArray() )
    }
    record.headers()
      .add( DungeonMessageHeaders.EVENT_ID, UUID.randomUUID().toString().encodeToByteArray() )
    record.headers()
      .add( DungeonMessageHeaders.TYPE, event::class.simpleName.toString().encodeToByteArray() )
    record.headers()
      .add( DungeonMessageHeaders.VERSION, "1".encodeToByteArray() )
    record.headers()
      .add( DungeonMessageHeaders.TIMESTAMP, dateTimeFormatter.format(Instant.now()).encodeToByteArray() )
    recipients.forEach {
      record.headers()
        .add( DungeonMessageHeaders.PLAYER_ID, it.toString().encodeToByteArray() )
    }
    return record
  }

  private fun getRecipients(event: RobotIntegrationEvent): List<UUID> {
    // These functions represent our "business logic" for determining who should receive the event
    // It can be seen as visibility policies
    val playersOfRobots: (List<UUID>) -> List<UUID> = { robotIds ->
      val robots = robotRepository.findAllById(robotIds)
      robots
        .map { it.player }
        .distinct()
    }
    val playerOfSingleRobot: (UUID) -> List<UUID> = {
      playersOfRobots(listOf(it))
    }
    // Our connector will resolve an empty list of players to all players
    val allPlayers: () -> List<UUID> = { emptyList() }

    // Pattern matching is exhaustive and will throw an exception if a new type is added
    return when (event) {
      is RobotMovedIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotRegeneratedIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotResourceMinedIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotResourceRemovedIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotRestoredAttributesIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotSpawnedIntegrationEvent -> playerOfSingleRobot(event.robot.id)
      is RobotUpgradedIntegrationEvent -> playerOfSingleRobot(event.robotId)
      is RobotAttackedIntegrationEvent -> playersOfRobots(
        listOf(
          event.attacker.robotId,
          event.target.robotId
        )
      )
      is RobotsRevealedIntegrationEvent -> allPlayers()
    }
  }
}
