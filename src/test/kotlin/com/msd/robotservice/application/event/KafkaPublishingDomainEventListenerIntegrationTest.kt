package com.msd.robotservice.application.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.msd.robotservice.AbstractIntegrationTest
import com.msd.robotservice.domain.robot.event.RobotMoved
import org.apache.kafka.clients.producer.ProducerRecord
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.kafka.core.KafkaTemplate
import java.util.*

internal class KafkaPublishingDomainEventListenerIntegrationTest : AbstractIntegrationTest() {
  @Autowired
  private lateinit var objectMapper: ObjectMapper

  @MockBean
  private lateinit var template: KafkaTemplate<String, String>

  @Autowired
  private lateinit var applicationEventPublisher: ApplicationEventPublisher

  @Test
  fun `should publish domainEvent from eventBus to Kafka`() {
    val playerId = UUID.randomUUID()
    val event = RobotMoved(playerId, UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID())
    whenever(template.send(any<ProducerRecord<String, String>>())).thenReturn(mock())

    // When
    applicationEventPublisher.publishEvent(event)

    // Then
    val captor = argumentCaptor<ProducerRecord<String, String>>()
    verify(template).send(captor.capture())
    assertThat(captor.allValues)
      .hasSize(1)

    val firstValue = captor.firstValue
    val softly = SoftAssertions()
    softly.assertThat(firstValue.key())
      .isEqualTo(event.robot.toString())
    softly.assertThat(firstValue.value())
      .isNotNull
    softly.assertThat(firstValue.headers())
      .anyMatch { it.key() == "eventId" && it.value().isNotEmpty() }
      .anyMatch { it.key() == "type" && it.value().isNotEmpty() }
      .anyMatch { it.key() == "version" && it.value().isNotEmpty() }
      .anyMatch { it.key() == "timestamp" && it.value().isNotEmpty() }
      .anyMatch { it.key() == "playerId" && it.value().contentEquals(playerId.toString().encodeToByteArray()) }
    softly.assertAll()
  }
}
