package com.msd.robotservice.application.robot.dtos

import java.util.*

data class RobotSpawnDto(
  val transactionId: UUID,
  val player: UUID,
  val quantity: Int
)
