package com.msd.robotservice.domain.robot.event

import java.util.*

data class RobotAttacked(
  private val playerIds: List<UUID>,
  val attacker: UUID,
  val defender: UUID,
) : RobotDomainEvent {
  override fun robotId(): UUID = attacker
  override fun playerIds(): List<UUID> = playerIds
}
