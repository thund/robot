package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.RestorationType
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotResourceMinedIntegrationEvent
import com.msd.robotservice.application.robot.event.RobotRestoredAttributesIntegrationEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.context.ApplicationEventPublisher
import java.util.*

internal class RestorationHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val robot1 = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
  private val eventPublisher = mock<RobotIntegrationEventPublisher>()
  private val restorationHandler = RestorationHandler(robotRepository, eventPublisher)

  @Test
  fun `should restore health`() {
    // Given
    val robot = robot1
    whenever(robotRepository.findByIdOrThrow(eq(robot.id))).thenReturn(robot)
    robot.receiveDamage(2)
    val transactionId = UUID.randomUUID()

    // When
    restorationHandler.handle(robot.id, RestorationType.HEALTH, transactionId)

    val captor = argumentCaptor<Robot>()
    verify(robotRepository).save(captor.capture())

    // Then
    Assertions.assertThat(captor.firstValue.health)
      .isEqualTo(10)
    verify(eventPublisher).publish(any<RobotRestoredAttributesIntegrationEvent>(), eq(transactionId))
  }

  @Test
  fun `should restore energy`() {
    // Given
    val robot = robot1
    whenever(robotRepository.findByIdOrThrow(eq(robot.id))).thenReturn(robot)
    whenever(robotRepository.save(any<Robot>())).thenAnswer { it.arguments[0] }
    robot.reduceEnergy(2)
    val transactionId = UUID.randomUUID()

    // When
    restorationHandler.handle(robot.id, RestorationType.ENERGY, transactionId)

    val captor = argumentCaptor<Robot>()
    verify(robotRepository).save(captor.capture())

    // Then
    Assertions.assertThat(captor.firstValue.energy)
      .isEqualTo(20)
    verify(eventPublisher).publish(any<RobotRestoredAttributesIntegrationEvent>(), eq(transactionId))
  }
}
