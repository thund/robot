package com.msd.robotservice.application.robot.event

import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotAttackedIntegrationEvent(
  val attacker: RobotFightResult,
  val target: RobotFightResult
) : RobotIntegrationEvent {

  companion object {
    fun build(attacker: Robot, target: Robot) =
      RobotAttackedIntegrationEvent(
        attacker = RobotFightResult(
          robotId = attacker.id,
          availableHealth = attacker.health,
          availableEnergy = attacker.energy,
          alive = attacker.alive
        ),
        target = RobotFightResult(
          robotId = target.id,
          availableHealth = target.health,
          availableEnergy = target.energy,
          alive = target.alive
        )
      )
  }

  data class RobotFightResult(
    val robotId: UUID,
    val availableHealth: Int,
    val availableEnergy: Int,
    val alive: Boolean
  )

  override fun key(): String = attacker.robotId.toString()
}
