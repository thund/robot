import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("org.springframework.boot") version "2.7.6"
  id("io.spring.dependency-management") version "1.1.0"
  id("org.jetbrains.kotlin.plugin.allopen") version "1.7.21"
  id("org.jetbrains.kotlin.plugin.noarg") version "1.7.21"
  // id("org.jlleitschuh.gradle.ktlint") version "11.0.0"
  id("com.google.cloud.tools.jib") version "3.3.1"
  id("org.jetbrains.dokka") version "1.7.20"
  // Activate whenever you need to analyze the task tree
  // id("com.dorongold.task-tree") version "2.1.0"
  idea
  jacoco
  id("maven-publish")
  kotlin("jvm") version "1.7.21"
  kotlin("plugin.spring") version "1.7.21"
  kotlin("plugin.jpa") version "1.7.21"
  kotlin("kapt") version "1.7.21"
}

group = "com.msd"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
  mavenCentral()
}

allOpen {
  annotation("javax.persistence.Entity")
  annotation("javax.persistence.MappedSuperclass")
  annotation("javax.persistence.Embeddable")
}

noArg {
  annotation("javax.persistence.Entity")
  annotation("javax.persistence.MappedSuperclass")
  annotation("javax.persistence.Embeddable")
}

val testcontainersVersion = "1.17.6"

dependencies {
  annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

  implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.14.1")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("org.mapstruct:mapstruct:1.5.3.Final")
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa")
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("org.springframework.kafka:spring-kafka")
  implementation("org.apache.kafka:kafka-streams")
  implementation("io.github.microutils:kotlin-logging-jvm:3.0.4")
  implementation("org.mariadb.jdbc:mariadb-java-client")
  implementation("com.github.ben-manes.caffeine:caffeine:3.1.2")

  kapt("org.mapstruct:mapstruct-processor:1.5.3.Final")
  runtimeOnly("com.h2database:h2")

  testImplementation("com.squareup.okhttp3:okhttp:4.10.0")
  testImplementation("com.squareup.okhttp3:mockwebserver:4.10.0")
  testImplementation(platform("org.junit:junit-bom:5.9.1"))
  testImplementation("org.junit.jupiter:junit-jupiter:5.9.1")

  testImplementation("org.springframework.boot:spring-boot-starter-test") {
    exclude("org.junit")
  }
  testImplementation("org.springframework.kafka:spring-kafka-test")
  testImplementation("io.mockk:mockk:1.13.3")
  testImplementation("org.mockito.kotlin:mockito-kotlin:4.1.0")
  testImplementation("org.awaitility:awaitility:4.2.0")

  testImplementation("org.testcontainers:testcontainers:$testcontainersVersion")
  testImplementation("org.testcontainers:junit-jupiter:$testcontainersVersion")
  testImplementation("org.testcontainers:redpanda:$testcontainersVersion")
}

// Kapt has a very terrible performance as it generates stubs. Therefore, it has been put in
// maintenance mode and the Kotlin Symbol Processing API (KSP) is the recommended tooling for
// running annotation processors. Unfortunately MapStruct does not support KSP as of now.
// Open Issue supporting KSP: https://github.com/mapstruct/mapstruct/issues/2522
// As MapStruct does code generation mostly independently we can still leverage some performance
// tweaks for kapt. As long as no other annotation processor is used the settings can remain safely.
// Note the `kapt.*` properties set in gradle.properties
// If there's any problem during annotation processing phase consider revisiting these tweaks
kapt {
  arguments {
    // Set Mapstruct Configuration options here
    // https://kotlinlang.org/docs/reference/kapt.html#annotation-processor-arguments
    // https://mapstruct.org/documentation/stable/reference/html/#configuration-options
    arg("mapstruct.defaultComponentModel", "spring")
  }
}

// KotlinCompile depends on JavaCompile so we can fork here
tasks.withType<JavaCompile>().configureEach {
  options.isFork = true
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=all")
    jvmTarget = "17"
  }
}

// TODO: Tests could run in parallel but are not concurrently written and would lead to test
//  inferences
tasks.withType<Test> {
  useJUnitPlatform()
  if (project.hasProperty("createReports")) {
    finalizedBy(tasks.jacocoTestReport)
  }
}

tasks.jacocoTestReport {
  dependsOn(tasks.test) // tests are required to run before generating the report
  reports {
    xml.required.set(false)
    csv.required.set(false)
    html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
  }
}

jacoco {
  // You may modify the Jacoco version here
  toolVersion = "0.8.7"
}

idea {
  module {
    isDownloadJavadoc = true
    isDownloadSources = true
  }
}

jib {
  from {
    image = "eclipse-temurin:17-alpine"
  }
  to {
    image = "registry.gitlab.com/the-microservice-dungeon/core-services/robot"
  }
}
