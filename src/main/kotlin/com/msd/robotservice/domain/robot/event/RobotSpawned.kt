package com.msd.robotservice.domain.robot.event

import com.msd.robotservice.domain.robot.Robot
import java.util.*

data class RobotSpawned(
  private val playerId: UUID,
  val robot: Robot
) : RobotDomainEvent {
  override fun robotId(): UUID = robot.id
  override fun playerIds(): List<UUID> = listOf(playerId)
}
