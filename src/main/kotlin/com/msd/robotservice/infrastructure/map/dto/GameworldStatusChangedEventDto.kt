package com.msd.robotservice.infrastructure.map.dto

import java.util.*

data class GameworldStatusChangedEventDto(
  val id: UUID,
  val status: GameworldStatus
)
