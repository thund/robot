package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotResourceMinedIntegrationEvent
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.LevelTooLowException
import com.msd.robotservice.infrastructure.map.GameMapService
import com.msd.robotservice.infrastructure.map.NoResourceOnPlanetException
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class MiningCommandHandler(
  val robotRepository: RobotRepository,
  val gameMapService: GameMapService,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {

  companion object {
    val logger = KotlinLogging.logger {}
  }

  @Transactional(rollbackOn = [Exception::class])
  fun handle(command: RobotCommand) {
    // In a version before mining commands were aggregated based on their mining levels and executed
    // as a whole "big" command. For example if two robots want to mine on planet A, their mining
    // speed was added and a request to the map service performed. This brought much complexity
    // into the process here. Although the intention to reduce networking costs is nice, it brings
    // way to many drawbacks even besides the complexity. For example it is map service
    // responsibility to take care about resource amounts, it can occur that the aggregated mining
    // command fails but a single command would work fine. (Planet has 1 Iron, Robot A requested 1,
    // Robot 2 requested 1)
    val robotId = command.robotId;
    val robot = this.robotRepository.findByIdOrThrow(robotId)
    val planet = robot.planet

    val resourceType = planet.resourceType
      ?: throw NoResourceOnPlanetException(planet.planetId)
    if (!robot.canMine(resourceType)) {
      throw LevelTooLowException("The mining level of the robot is too low to mine the resource $resourceType")
    }

    val amountToMine = robot.miningSpeed

    // Just delegate the mining process to the map service.
    val response = gameMapService.mine(planet.planetId, amountToMine).block()!!
    robot.addResource(resourceType, response)
    this.robotRepository.save(robot)

    val event = RobotResourceMinedIntegrationEvent.build(robot, resourceType, response)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
