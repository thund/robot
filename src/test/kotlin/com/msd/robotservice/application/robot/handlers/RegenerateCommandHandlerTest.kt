package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotRegeneratedIntegrationEvent
import com.msd.robotservice.application.robot.event.RobotResourceMinedIntegrationEvent
import com.msd.robotservice.application.robot.exception.TargetPlanetNotReachableException
import com.msd.robotservice.application.robot.exception.UnknownPlanetException
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import com.msd.robotservice.infrastructure.map.GameMapService
import com.msd.robotservice.infrastructure.map.NoResourceOnPlanetException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.context.ApplicationEventPublisher
import reactor.core.publisher.Mono
import java.util.*

internal class RegenerateCommandHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val eventPublisher = mock<RobotIntegrationEventPublisher>()
  private val regenerateCommandHandler = RegenerateCommandHandler(robotRepository, eventPublisher)

  @Test
  fun shouldRegenerate() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val robot = Robot.of(UUID.randomUUID(), planet)
    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)

    robot.reduceEnergy(10)
    val energyBefore = robot.energy

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.REGENERATE,
      null,
      null
    )

    regenerateCommandHandler.handle(command)
    assertThat(robot.energy)
      .isGreaterThan(energyBefore)
    verify(robotRepository).save(robot)
    verify(eventPublisher).publish(any<RobotRegeneratedIntegrationEvent>(), eq(command.transactionId))
  }
}
