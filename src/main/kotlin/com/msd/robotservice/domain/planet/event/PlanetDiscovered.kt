package com.msd.robotservice.domain.planet.event

import com.msd.robotservice.domain.planet.Planet

data class PlanetDiscovered(
  val planet: Planet
) : PlanetDomainEvent
