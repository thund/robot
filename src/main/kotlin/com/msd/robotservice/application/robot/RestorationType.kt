package com.msd.robotservice.application.robot

enum class RestorationType {
  HEALTH, ENERGY
}
