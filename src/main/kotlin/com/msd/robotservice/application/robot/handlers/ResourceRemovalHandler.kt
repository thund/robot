package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.RestorationType
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotResourceRemovedIntegrationEvent
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResourceRemovalHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }
  fun handle(robotId: UUID, resourceType: ResourceType, amount: Int, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    robot.takeResources(resourceType, amount)
    this.robotRepository.save(robot)

    val event = RobotResourceRemovedIntegrationEvent.build(robot, resourceType, amount)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
