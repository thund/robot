package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotUpgradedIntegrationEvent
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.UpgradeType
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class UpgradeRobotHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(robotId: UUID, upgradeType: UpgradeType, level: Int, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    robot.upgrade(upgradeType, level)
    logger.info("Successfully upgraded $upgradeType of robot $robotId")
    this.robotRepository.save(robot)

    val event = RobotUpgradedIntegrationEvent.build(robot, upgradeType, level)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
