package com.msd.robotservice.application.robot.dtos

data class InventoryDto(
  val maxStorage: Int,
  val usedStorage: Int,
  val storedCoal: Int,
  val storedIron: Int,
  val storedGem: Int,
  val storedGold: Int,
  val storedPlatin: Int
)
