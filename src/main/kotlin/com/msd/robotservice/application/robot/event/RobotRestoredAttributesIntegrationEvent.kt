package com.msd.robotservice.application.robot.event

import com.msd.robotservice.application.robot.RestorationType
import com.msd.robotservice.domain.robot.Robot
import java.util.UUID

data class RobotRestoredAttributesIntegrationEvent(
  val robotId: UUID,
  val restorationType: RestorationType,
  val availableEnergy: Int,
  val availableHealth: Int,
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot, restorationType: RestorationType) =
      RobotRestoredAttributesIntegrationEvent(
        robotId = robot.id,
        restorationType = restorationType,
        availableEnergy = robot.energy,
        availableHealth = robot.health,
      )
  }

  override fun key(): String = robotId.toString()
}
