[![pipeline status](https://gitlab.com/the-microservice-dungeon/robot/badges/main/pipeline.svg)](https://gitlab.com/the-microservice-dungeon/robot/-/commits/main) |
[![coverage report](https://gitlab.com/the-microservice-dungeon/robot/badges/main/coverage.svg)](https://gitlab.com/the-microservice-dungeon/robot/-/commits/main) |
[Test Coverage Report](https://gitlab.com/the-microservice-dungeon/robot/-/jobs/artifacts/main/file/build/jacocoHtml/index.html?job=test) |
[Code Quality Report](https://the-microservice-dungeon.gitlab.io/robot)

___

## Running using Local Dev Env

The easiest way to start development is by using the [local-dev-env](https://gitlab.com/the-microservice-dungeon/local-dev-environment).
Which provides you with a full environment to develop the service.
However, the local-dev-environment will also start a service by itself which blocks the port.
Therefore, make sure to don't start the game service by following the instructions in the local-dev-env
documentation.

This README file should be adapted to include the following information:

(1) How can the service be started, regardless of the platform? A short, precise instruction is sufficient. Existence/familiarity with tools like maven can be assumed.

(2) What other prerequisites must be in place for the service to run? For example, linking to further documentation of required infrastructure such as Kafka from the DevOps team.

(3) Are there decisions from the decision log that should be further elaborated (like domain model)? Depending on the level of detail, describe in more detail here or in the wiki.

(4) Are there further links that promote navigation or understanding? For example, link to API documentation or important decisions in the decision log.

_Note_: The design of the task planning and review is up to the team. One possible option is to use a Kanban board directly via GitHub.
