# helm-robot


helm chart for robot


run following on folder above

```
helm install robot helm-chart --namespace robot --create-namespace
```

OR from registry

```
helm install robot dungeon/robot --namespace robot --create-namespace
```

service will be exposed at port `30004` after `minikube tunnel`