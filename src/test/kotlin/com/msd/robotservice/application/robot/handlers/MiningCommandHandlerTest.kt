package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotResourceMinedIntegrationEvent
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import com.msd.robotservice.infrastructure.map.GameMapService
import com.msd.robotservice.infrastructure.map.NoResourceOnPlanetException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.context.ApplicationEventPublisher
import reactor.core.publisher.Mono
import java.util.*

internal class MiningCommandHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val gameMapService = mock<GameMapService>()
  private val eventPublisher = mock<RobotIntegrationEventPublisher>()
  private val miningCommandHandler = MiningCommandHandler(robotRepository, gameMapService, eventPublisher)

  @Test
  fun shouldThrowWhenNoResource() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val robot = Robot.of(UUID.randomUUID(), planet)

    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.MINING,
      null,
      null
    )

    assertThatThrownBy { miningCommandHandler.handle(command) }
      .isInstanceOf(NoResourceOnPlanetException::class.java)
  }

  @Test
  fun shouldExecuteMining() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    planet.resourceType = ResourceType.COAL
    val robot = Robot.of(UUID.randomUUID(), planet)

    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)
    whenever(gameMapService.mine(eq(planet.planetId), any<Int>())).thenAnswer { Mono.just(it.arguments[1]) }

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.MINING,
      null,
      null
    )

    miningCommandHandler.handle(command)

    assertThat(robot.inventory.resources[ResourceType.COAL]).isGreaterThan(0)
    verify(robotRepository).save(robot)
    verify(eventPublisher).publish(any<RobotResourceMinedIntegrationEvent>(), eq(command.transactionId))
  }
}
