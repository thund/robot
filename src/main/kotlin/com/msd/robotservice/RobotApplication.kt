package com.msd.robotservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class RobotApplication

fun main(args: Array<String>) {
  runApplication<RobotApplication>(*args)
}
