package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.command.RobotCommand
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotMovedIntegrationEvent
import com.msd.robotservice.application.robot.event.RobotResourceMinedIntegrationEvent
import com.msd.robotservice.application.robot.exception.TargetPlanetNotReachableException
import com.msd.robotservice.application.robot.exception.UnknownPlanetException
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import com.msd.robotservice.infrastructure.map.GameMapService
import com.msd.robotservice.infrastructure.map.NoResourceOnPlanetException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.context.ApplicationEventPublisher
import reactor.core.publisher.Mono
import java.util.*

internal class MovementCommandHandlerTest {
  private val robotRepository = mock<RobotRepository>()
  private val planetRepository = mock<PlanetRepository>()
  private val eventPublisher = mock<RobotIntegrationEventPublisher>()
  private val movementCommandHandler = MovementCommandHandler(robotRepository, planetRepository, eventPublisher)

  @Test
  fun shouldThrowWhenNotReachable() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val otherPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val robot = Robot.of(UUID.randomUUID(), planet)

    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.MOVEMENT,
      otherPlanet.planetId,
      null
    )

    assertThatThrownBy { movementCommandHandler.handle(command) }
      .isInstanceOf(TargetPlanetNotReachableException::class.java)
  }

  @Test
  fun shouldThrowWhenUnknown() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val otherPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    planet.addNeighbour(otherPlanet)

    val robot = Robot.of(UUID.randomUUID(), planet)
    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)
    whenever(planetRepository.findById(otherPlanet.planetId)).thenReturn(Optional.empty())

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.MOVEMENT,
      otherPlanet.planetId,
      null
    )

    assertThatThrownBy { movementCommandHandler.handle(command) }
      .isInstanceOf(UnknownPlanetException::class.java)
  }

  @Test
  fun shouldExecuteMovement() {
    val planet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    val otherPlanet = Planet.of(UUID.randomUUID(), UUID.randomUUID())
    planet.addNeighbour(otherPlanet)

    val robot = Robot.of(UUID.randomUUID(), planet)
    whenever(robotRepository.findByIdOrThrow(robot.id)).thenReturn(robot)
    whenever(planetRepository.findById(otherPlanet.planetId)).thenReturn(Optional.of(otherPlanet))

    val command = RobotCommand(
      robot.id,
      UUID.randomUUID(),
      robot.player,
      RobotCommand.CommandType.MOVEMENT,
      otherPlanet.planetId,
      null
    )

    movementCommandHandler.handle(command)
    assertThat(robot.planet).isEqualTo(otherPlanet)
    verify(robotRepository).save(robot)
    verify(eventPublisher).publish(any<RobotMovedIntegrationEvent>(), eq(command.transactionId))
  }
}
