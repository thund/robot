package com.msd.robotservice.application.planet

import com.msd.robotservice.domain.planet.PlanetType
import com.msd.robotservice.domain.planet.ResourceType
import java.util.*

data class PlanetDto(
  val planetId: UUID,
  val movementDifficulty: Int,
  val planetType: PlanetType?,
  val resourceType: ResourceType?
)
