package com.msd.robotservice.application.robot.exception

import com.msd.robotservice.application.core.FailureException
import java.util.*

class UnknownGameworldException(gameworld: UUID) :
  FailureException("Map Service doesn't have a gameworld with the id $gameworld")
