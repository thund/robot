package com.msd.robotservice.application.debug.mode

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling

/**
 * Configuration class for the debug mode
 */
@Configuration
@Profile("debug")
@EnableScheduling
class DbgConfiguration
