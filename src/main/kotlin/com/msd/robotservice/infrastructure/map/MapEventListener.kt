package com.msd.robotservice.infrastructure.map

import com.fasterxml.jackson.databind.ObjectMapper
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.RobotRepository
import com.msd.robotservice.infrastructure.map.dto.GameworldCreatedEventDto
import com.msd.robotservice.infrastructure.map.dto.GameworldStatus
import com.msd.robotservice.infrastructure.map.dto.GameworldStatusChangedEventDto
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.support.TransactionTemplate
import kotlin.math.abs

@Component
class MapEventListener(
  val objectMapper: ObjectMapper,
  val planetRepository: PlanetRepository,
  val robotRepository: RobotRepository,
  val platformTransactionManager: PlatformTransactionManager,
  val transactionTemplate: TransactionTemplate = TransactionTemplate(platformTransactionManager)
) {
  val logger = KotlinLogging.logger { }

  // TODO: At the moment we are using a programmatic approach to handle the events based on the type
  //  information in the header. It is relatively easy to understand and does not use spring magic,
  //  but there might be a cleaner option.
  @KafkaListener(topics = ["gameworld"], groupId = "robot", autoStartup = "true")
  fun gameworldTopicListener(
    @Header("type") type: String,
    record: ConsumerRecord<String, ByteArray>
  ) {
    when (type) {
      "GameworldCreated" -> {
        logger.info { "Received gameworld-created event: ${record.headers()}" }
        handleGameworldCreated(record)
      }

      "GameworldStatusChanged" -> {
        logger.info { "Received gameworld-status-changed event: ${record.headers()}" }
        handleGameworldStatusChanged(record)
      }

      else -> {
        logger.error { "Unknown type header found: ${type}" }
      }
    }
  }

  // TODO: Relatively(!) slow (doesn't matter if we look at the throughput of the topic).
  //       But because of the back-references we cannot drop repository calls here
  //       as otherwise we are likely to get a StackOverflow error. It might be a good idea to
  //       overthink the design decision to have a back-reference to the planet neighbours.
  private fun handleGameworldCreated(record: ConsumerRecord<String, ByteArray>) {
    val eventDto = objectMapper.readValue(record.value(), GameworldCreatedEventDto::class.java)
    logger.info { "Received gameworld-created event with ${eventDto.planets.size} planets" }

    // First save every planet
    val planets = eventDto.planets.map { planet ->
      Planet.of(
        planet.id,
        eventDto.id,
        planet.movementDifficulty,
        planet.resource?.resourceType
      )
    }
    planetRepository.saveAll(planets)

    // Now find the neighbours for each planet
    val neighbourPlanets = eventDto.planets.map {
      val planet = planetRepository.findById(it.id).get()
      val neighbours = eventDto.planets
        .filter { p -> (abs(p.x - it.x) <= 1 && p.y == it.y) || (abs(p.y - it.y) <= 1 && p.x == it.x) }
        .filter { p -> p.id != it.id } // Don't add the planet itself as neighbour
        .map { p -> planets.first { p.id == it.planetId } }
      neighbours.forEach { planet.addNeighbour(it) }
      planet
    }
    val savedPlanets = planetRepository.saveAll(neighbourPlanets).toSet()

    logger.info { "Saved ${savedPlanets.size} planets" }
  }

  // TODO: We can't use the declarative transactiona mangement here.
  //       I don't know exactly why.
  // @Transactional(Transactional.TxType.REQUIRES_NEW)
  fun handleGameworldStatusChanged(record: ConsumerRecord<String, ByteArray>) {
    val eventDto = objectMapper.readValue(record.value(), GameworldStatusChangedEventDto::class.java)

    if(eventDto.status != GameworldStatus.INACTIVE)
      return

    val tx = platformTransactionManager.getTransaction(transactionTemplate)
    robotRepository.deleteAllRobotsInGameworld(eventDto.id)
    planetRepository.deleteAllPlanetsInGameworld(eventDto.id)
    platformTransactionManager.commit(tx)
  }
}
