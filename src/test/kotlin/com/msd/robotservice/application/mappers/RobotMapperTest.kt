package com.msd.robotservice.application.mappers

import com.msd.robotservice.application.robot.mappers.RobotMapper
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.Robot
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.util.*

@SpringBootTest
@ActiveProfiles(profiles = ["test"])
internal class RobotMapperTest(
  @Autowired
  val robotMapper: RobotMapper
) {
  private lateinit var robot: Robot

  @BeforeEach
  fun init() {
    robot = Robot.of(UUID.randomUUID(), Planet.of(UUID.randomUUID(), UUID.randomUUID()))
    robot.inventory.addResource(ResourceType.COAL, 1)
    robot.inventory.addResource(ResourceType.IRON, 2)
    robot.inventory.addResource(ResourceType.GEM, 3)
    robot.inventory.addResource(ResourceType.GOLD, 4)
    robot.inventory.addResource(ResourceType.PLATIN, 5)
  }

  @Test
  fun `when converting a robot to RobotDTO all values are correctly set`() {
    val robotDto = robotMapper.robotToRobotDto(robot)

    assertAll(
      "All values correctly mapped",
      {
        assertEquals(robot.id, robotDto.id)
      },
      {
        assertEquals(robot.alive, robotDto.alive)
      },
      {
        assertEquals(1, robotDto.inventory.storedCoal)
      },
      {
        assertEquals(2, robotDto.inventory.storedIron)
      },
      {
        assertEquals(3, robotDto.inventory.storedGem)
      },
      {
        assertEquals(4, robotDto.inventory.storedGold)
      },
      {
        assertEquals(5, robotDto.inventory.storedPlatin)
      }
    )
  }
}
