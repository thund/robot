package com.msd.robotservice.application.robot.event

import java.util.UUID

interface RobotIntegrationEventPublisher {
    fun publish(
      event: RobotIntegrationEvent,
      transactionId: UUID? = null,
    )
}
