package com.msd.robotservice.domain.robot

import com.msd.robotservice.domain.core.AbstractAggregateRoot
import com.msd.robotservice.domain.planet.Planet
import com.msd.robotservice.domain.planet.ResourceType
import com.msd.robotservice.domain.robot.event.*
import com.msd.robotservice.domain.robot.exception.NotEnoughEnergyException
import com.msd.robotservice.domain.robot.exception.TargetRobotOutOfReachException
import com.msd.robotservice.domain.robot.exception.UpgradeException
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

/**
 * Robot Domain Object
 *
 * Note that the constructor is intended to be private to use the fabric method.
 */
@Entity
class Robot private constructor(
  @Type(type = "uuid-char")
  val player: UUID,
  planet: Planet
) : AbstractAggregateRoot() {

  companion object {
    /**
     * Fabric method to create a new robot.
     * Adds creation event.
     */
    fun of(player: UUID, planet: Planet): Robot {
      val robot = Robot(player, planet)
      robot.events.add(RobotSpawned(player, robot))
      return robot
    }
  }

  @Id
  @Type(type = "uuid-char")
  val id: UUID = UUID.randomUUID()

  @ManyToOne(cascade = [CascadeType.MERGE])
  var planet = planet
    protected set

  var alive: Boolean = true

  val maxHealth
    @Transient
    get() = UpgradeValues.maxHealthByLevel[healthLevel]

  val maxEnergy
    @Transient
    get() = UpgradeValues.maxEnergyByLevel[energyLevel]

  val energyRegen
    @Transient
    get() = UpgradeValues.energyRegenByLevel[energyRegenLevel]

  val attackDamage: Int
    @Transient
    get() = UpgradeValues.attackDamageByLevel[damageLevel]

  @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
  val inventory = Inventory()

  val miningSpeed: Int
    @Transient
    get() = UpgradeValues.miningSpeedByLevel[miningSpeedLevel]

  var health: Int = this.maxHealth
    protected set(value) {
      field = if (value > this.maxHealth)
        this.maxHealth
      else
        value
    }

  var energy: Int = this.maxEnergy
    protected set(value) {
      field = if (value > this.maxEnergy)
        this.maxEnergy
      else
        value
    }

  /**
   * Sets the robot's healthLevel to the given value.
   *
   * @throws UpgradeException    when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var healthLevel: Int = 0
    protected set(value) {
      if (value > 5) throw UpgradeException("Max Health Level has been reached. Upgrade not possible.")
      else if (value > healthLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $healthLevel to level $value"
        )
      else if (value <= healthLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $healthLevel to level $value")
      val diff = UpgradeValues.maxHealthByLevel[value] - UpgradeValues.maxHealthByLevel[field]
      field = value
      health += diff
    }

  /**
   * Sets the robot's damageLevel to the given value.
   *
   * @throws UpgradeException when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var damageLevel: Int = 0
    protected set(value) {
      if (value > 5) throw UpgradeException("Max Damage Level has been reached. Upgrade not possible.")
      else if (value > damageLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. " +
            "Tried to upgrade from level $damageLevel to level $value"
        )
      else if (value <= damageLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $damageLevel to level $value")
      field = value
    }

  /**
   * Sets the robot's miningSpeedLevel to the given value.
   *
   * @throws UpgradeException when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var miningSpeedLevel: Int = 0
    protected set(value) {
      if (value > 5) throw UpgradeException("Max MiningSpeed Level has been reached. Upgrade not possible.")
      else if (value > miningSpeedLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $miningSpeedLevel to level $value"
        )
      else if (value <= miningSpeedLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $miningSpeedLevel to level $value")
      field = value
    }

  /**
   * Sets the robot's miningLevel to the given value.
   *
   * @throws UpgradeException when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var miningLevel: Int = 0
    protected set(value) {
      if (value > 4) throw UpgradeException("Max Mining Level has been reached. Upgrade not possible.")
      else if (value > miningLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $miningLevel to level $value"
        )
      else if (value <= miningLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $miningLevel to level $value")
      field = value
    }

  /**
   * Sets the robot's energyLevel to the given value.
   *
   * @throws UpgradeException when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var energyLevel: Int = 0
    protected set(value) {
      if (value > 5) throw UpgradeException("Max Energy Level has been reached. Upgrade not possible.")
      else if (value > energyLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $energyLevel to level $value"
        )
      else if (value <= energyLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $energyLevel to level $value")
      val diff = UpgradeValues.maxEnergyByLevel[value] - UpgradeValues.maxEnergyByLevel[field]
      field = value
      energy += diff
    }

  /**
   * Sets the robot's energyRegenLevel to the given value.
   *
   * @throws UpgradeException when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  var energyRegenLevel: Int = 0
    protected set(value) {
      if (value > 5) throw UpgradeException("Max Energy Regen Level has been reached. Upgrade not possible.")
      else if (value > energyRegenLevel + 1)
        throw UpgradeException(
          "Cannot skip upgrade levels. Tried to upgrade from level $energyRegenLevel to level $value"
        )
      else if (value <= energyRegenLevel)
        throw UpgradeException("Cannot downgrade Robot. Tried to go from level $energyRegenLevel to level $value")
      field = value
    }

  /**
   * Moves this `Robot` to a given `Planet`, unless the `Robots` current `Planet` is blocked.
   *
   * @param planet The `Planet` to move to.
   * @param cost The cost of moving to the `Planet`, which is the movement difficulty of the target.
   * @throws NotEnoughEnergyException if the `Robot` does not have enough energy to move to the `Planet`.
   */
  fun move(planet: Planet) {
    this.reduceEnergy(planet.movementDifficulty)
    val fromPlanet = this.planet.planetId
    this.planet = planet
    this.events.add(RobotMoved(this.player, this.id, fromPlanet, planet.planetId))
  }

  /**
   * This `Robot` receives a given amount of [damage].
   * If the damage reduces the health to (or below) 0, the robot is destroyed.
   *
   * @param damage the amount of damage to be received
   */
  fun receiveDamage(damage: Int) {
    if (damage < 0) throw IllegalArgumentException("Receive damage amount cannot be less than zero")
    this.health -= damage
    this.events.add(RobotHealthUpdated(this.player, this.id, -damage, this.health))
    if (health <= 0) this.kill()
  }

  /**
   * Kills the robot
   */
  fun kill() {
    this.alive = false
    this.events.add(RobotKilled(this.player, this.id))
  }

  /**
   * Reduces the energy of the `Robot` by the given amount.
   *
   * @param amount the amount of energy to be subtracted
   * @throws NotEnoughEnergyException if the `Robots` current energy is less than the amount to be subtracted
   * @throws IllegalArgumentException if the amount to be subtracted is negative
   */
  fun reduceEnergy(amount: Int) {
    if (amount > energy) throw NotEnoughEnergyException("Tried to reduce energy by $amount but only has $energy energy")
    if (amount < 0) throw IllegalArgumentException("Used energy amount cannot be less than zero")

    energy -= amount
    this.events.add(RobotEnergyUpdated(this.player, this.id, -amount, energy))
  }

  /**
   * This `Robot` attacks another `Robot`.
   *
   * @param otherRobot the [Robot] to attack
   * @throws NotEnoughEnergyException if the robot has not enough energy to attack
   */
  fun attack(otherRobot: Robot) {
    if (!otherRobot.alive)
      throw IllegalArgumentException("Cannot attack a robot that is already dead")
    if (this.planet.planetId != otherRobot.planet.planetId)
      throw TargetRobotOutOfReachException("The attacking robot and the defending robot are not on the same planet")
    reduceEnergy(damageLevel + 1)
    otherRobot.receiveDamage(attackDamage)
    this.events.add(RobotAttacked(listOf(this.player, otherRobot.player), this.id, otherRobot.id))
  }

  /**
   * Changes the level of an Upgrade of this `Robot`. Upgrade levels can't be skipped, downgrades are not allowed and
   * the max level for all upgrades is 5 except for `Mining` which is 4. If one of those cases appears an Exception
   * is thrown.
   *
   * @param upgradeType the stat which should be updated
   * @param level       the level to which an upgrade should be upgraded
   * @throws UpgradeException    when an upgrade level is skipped, a downgrade is attempted or an upgrade past the max
   */
  fun upgrade(upgradeType: UpgradeType, level: Int) {
    when (upgradeType) {
      UpgradeType.DAMAGE -> damageLevel = level
      UpgradeType.ENERGY_REGEN -> energyRegenLevel = level
      UpgradeType.MAX_ENERGY -> energyLevel = level
      UpgradeType.HEALTH -> healthLevel = level
      UpgradeType.STORAGE -> inventory.storageLevel = level
      UpgradeType.MINING_SPEED -> miningSpeedLevel = level
      UpgradeType.MINING -> miningLevel = level
    }
    this.events.add(RobotUpgraded(this.player, this.id, upgradeType, level))
  }

  /**
   * Checks if this robot has a high enough level to mine the given resource.
   *
   * @param resourceType the type of resource which should be mined
   * @return if this robot can mine the given [resourceType]
   */
  fun canMine(resourceType: ResourceType): Boolean {
    return this.miningLevel >= resourceType.requiredMiningLevel
  }

  /**
   * @return the sum of all upgrades of this this [Robot]
   */
  fun totalUpgrades(): Int {
    return damageLevel + energyLevel + energyRegenLevel + healthLevel + inventory.storageLevel + miningSpeedLevel + miningLevel
  }

  /**
   * Regenerates this [Robot's] [Robot] `energy`. The amount restored corresponds to the `energyRegen` value.
   */
  fun regenerate() {
    this.regenerateBy(this.energyRegen)
  }

  /**
   * Fully restores a `Robot's` `energy`. This method unlike `regenerateEnergy` is supposed to represent the energy
   * regeneration at a space station.
   */
  fun restoreEnergy() {
    val toRegenerate = this.maxEnergy - this.energy
    regenerateBy(toRegenerate)
  }

  private fun regenerateBy(amount: Int) {
    this.energy += amount
    this.events.add(RobotEnergyUpdated(this.player, this.id, amount, this.energy))
  }

  /**
   * Repairs this [Robot] to full health.
   */
  fun repair() {
    repairBy(this.maxHealth - this.health)
  }

  fun repairBy(repairAmount: Int) {
    this.health += repairAmount
    this.events.add(RobotHealthUpdated(this.player, this.id, repairAmount, this.health))
  }

  fun addResource(resource: ResourceType, amount: Int) {
    this.inventory.addResource(resource, amount)
    this.events.add(RobotResourceMined(this.player, this.id, resource, amount))
    this.events.add(RobotInventoryUpdated(this.player, this.id, this.inventory))
  }

  fun takeResources(resource: ResourceType, amount: Int) {
    this.inventory.takeResource(resource, amount)
    this.events.add(RobotInventoryUpdated(this.player, this.id, this.inventory))
  }

  fun takeAllResources(): MutableMap<ResourceType, Int> {
    val result = this.inventory.takeAllResources()
    this.events.add(RobotInventoryUpdated(this.player, this.id, this.inventory))
    return result
  }
}
