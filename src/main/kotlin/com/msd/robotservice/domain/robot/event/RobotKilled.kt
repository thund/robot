package com.msd.robotservice.domain.robot.event

import java.util.*

data class RobotKilled(
  private val playerId: UUID,
  val robot: UUID
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
