package com.msd.robotservice.application.robot.handlers

import com.msd.robotservice.application.robot.RestorationType
import com.msd.robotservice.application.robot.event.RobotIntegrationEventPublisher
import com.msd.robotservice.application.robot.event.RobotRestoredAttributesIntegrationEvent
import com.msd.robotservice.domain.planet.PlanetRepository
import com.msd.robotservice.domain.robot.Robot
import com.msd.robotservice.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class RestorationHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(robotId: UUID, type: RestorationType, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    when (type) {
      RestorationType.ENERGY -> robot.restoreEnergy()
      RestorationType.HEALTH -> robot.repair()
    }
    this.robotRepository.save(robot)

    val event = RobotRestoredAttributesIntegrationEvent.build(robot, type)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
